package com.example.timesup;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.timesup.retrofit.EmployeeControler;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EmployeeControler controler = new EmployeeControler();
        controler.start();

    }
}
