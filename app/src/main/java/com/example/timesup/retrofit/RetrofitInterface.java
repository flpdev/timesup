package com.example.timesup.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RetrofitInterface {

    @GET("/employee/{id}")
    Call<Employee> getEmployee(@Path("id") int id);
}
