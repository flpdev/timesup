package com.example.timesup.retrofit;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmployeeControler implements Callback<Employee> {

    private static final String BASE_URL = "https://whispering-coast-93809.herokuapp.com/";
    private static final String TAG = EmployeeControler.class.getName();

    public void start() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RetrofitInterface gerritAPI = retrofit.create(RetrofitInterface.class);

        Call<Employee> call = gerritAPI.getEmployee(4);
        call.enqueue(this);

    }

    @Override
    public void onResponse(Call<Employee> call, Response<Employee> response) {
        if(response.isSuccessful()){
            Employee e =response.body();
            Log.d(TAG, e.getName() + e.getID() + e.getSalary());
        }else{
            Log.d(TAG, "response unsuccessful");
        }
    }

    @Override
    public void onFailure(Call<Employee> call, Throwable t) {
        //NO OBJECT WITH SUCH ID
        t.printStackTrace();
        Log.d(TAG, "FAILURE");
    }
}
